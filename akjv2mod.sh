#!/bin/bash
rm -r ENG-B-AKJV2018-pd-PSFM
git clone https://github.com/BibleCorps/ENG-B-AKJV2018-pd-PSFM.git
mv ENG-B-AKJV2018-pd-PSFM/p.sfm/ usfm
wget https://raw.githubusercontent.com/adyeths/u2o/master/u2o.py
chmod +x u2o.py
rm usfm/ENG\[B\]AKJV2018\[PD\]00-FRT.p.sfm
rm usfm/ENG[B]AKJV2018[PD]40-INT.p.sfm
rm usfm/ENG[B]AKJV2018[PD]95-GLO.p.sfm
cd usfm
sed -i 's/^\\*.*~//g' *.sfm
sed -i '/^$/d' *.sfm
sed -i 's/mt7/mt2/g;s/toc0/toc/g;s/\\q0/\\q/;s/\\b2|\\b3/\\b/g' *.sfm
sed -i 's/\\b[2-3]/\\b/g' *sfm
cd ../
./u2o.py -e utf-8 -l en -o akjv.osis.xml -v -d AKJV usfm/*.sfm

cat >> akjv.conf<< EOF
[AKJV]
DataPath=./modules/texts/ztext/akjv/
Description=American King James Version
Lang=en
Version=2.1
History_2.1= Issue #6 #8 #10 #13 (2023-12-24)
History_2.0=Updated TextSource from github, correction of an error in Exod 23:17, MOD-182 (2023-10-30)
History_1.4=Updated TextSource
History_1.3=compressed module
History_1.2=Updated to new text and fixed a couple incomplete verses.
About=American King James Version\par Produced by Stone Engelbrite\par\par This is a new translation of the Bible, based on the original King James Version. It is a simple word for word update from the King James English. I have taken care to change nothing doctrinely, but to simply update the spelling and vocabulary. I have not changed the grammar because that could alter it doctrinely.\par\par I am hereby putting the American King James version of the Bible into the public domain on November 8, 1999.\par\par       Michael Peter (Stone) Engelbrite\par\par You may use it in any manner you wish: copy it, sell it, modify it, etc.\par You can't copyright it or prevent others from using it.\par You can't claim that you created it, because you didn't.
TextSource=https://github.com/BibleCorps/ENG-B-AKJV2018-pd-PSFM
LCSH=Bible. English.
DistributionLicense=Copyrighted; Free non-commercial distribution
BlockType=BOOK
CompressType=ZIP
ModDrv=zText
SwordVersionDate=2023-10-30
InstallSize=1645088
EOF

